/*
 * This is a OOP assignment : java swing : components self-learning 
 * B.Sc. Computer Science, Burapha University
 */
package com.anurak.swingsimpleprogram;

import java.util.Random;

/**
 *
 * @author anurak
 */
public class Role {
    // 0 = Rock, 1 = Paper, 2 = Scissors
    // 0 = draw, 1 = player, 2 = com
    private static int randomCom(){
        Random rd = new Random();
        return rd.nextInt(3);
    }
    
    public static int[] result(int playerChoice){
        int comChoice = randomCom();
        // result[com, result]
        return new int[]{comChoice, role(comChoice, playerChoice)};
    }
    
    private static int role(int comChoice, int playerChoice){
        
        if (comChoice == playerChoice) return 0;
        
        switch(playerChoice){
            case 0:
                switch(comChoice){
                    case 1:
                        return 2;
                    case 2:
                        return 1; 
                }
            case 1:
                switch(comChoice){
                    case 0:
                        return 1;
                    case 2:
                        return 2; 
                }
            case 2:
                switch(comChoice){
                    case 0:
                        return 2;
                    case 1:
                        return 1; 
                }
        }
        return 0; 
    }
}
